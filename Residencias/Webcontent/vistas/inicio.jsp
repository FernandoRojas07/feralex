<!doctype html>
<html lang="en">
<head>
  	<title>Sidebar 01</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700,800,900" rel="stylesheet">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="<%=request.getContextPath()%>/vistas/css/style.css">
</head>
  <body>
		<div class="wrapper d-flex align-items-stretch">
			<nav  id="sidebar">
				<div class="p-4 pt-5">
		  		<a href="#" class="img logo rounded mb-5" style="background-image: url(images/logo.jpg);"></a>
	        <ul class="list-unstyled components mb-5">
	          <li class="active">
              <a href="#">Home</a>
	          </li>
	          <li>
	            <a href="#residenciasSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Residencias</a>
	            <ul class="collapse list-unstyled" id="residenciasSubmenu">
                <li>
                    <a href="#">Todas las residencias</a>
                </li>
                <li>
                    <a href="#">Agregar residencia</a>
                </li>
                <li>
                    <a href="#">Buscar residencia</a>
                </li>
	            </ul>
	          </li>
	          <li>
              <a href="#propietariosSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Propietarios</a>
              <ul class="collapse list-unstyled" id="propietariosSubmenu">
                <li>
                    <a href="<%=request.getContextPath()%>/vistas/js/todosPropietarios.jsp">Todos los propietarios 1</a>
                </li>
                <li>
                    <a href="<%=request.getContextPath()%>/vistas/js/agregaPropietario.jsp">Agregar propietario</a>
                </li>
                <li>
                    <a href="<%=request.getContextPath()%>/vistas/js/buscaPropietario.jsp">Buscar propietario</a>
                </li>
              </ul>
	          </li>
	          <li>
              <a href="#arrendatariosSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Arrendatarios</a>
              <ul class="collapse list-unstyled" id="arrendatariosSubmenu">
                <li>
                    <a href="<%=request.getContextPath()%>/vistas/js/todosArrendatarios.jsp">Todos los arrendatarios</a>
                </li>
                <li>
                    <a href="<%=request.getContextPath()%>/vistas/js/agregaArrendatario.jsp">Agregar arrendatarios</a>
                </li>
                <li>
                    <a href="<%=request.getContextPath()%>/vistas/js/buscaArrendatario.jsp">Buscar arrendatarios</a>
                </li>
              </ul>
	          </li>
	          <li>
              <a href="#automovilesSubmenu" data-toggle="collapse" aria-expanded="false" class="dropdown-toggle">Automoviles</a>
              <ul class="collapse list-unstyled" id="automovilesSubmenu">
                <li>
                    <a href="<%=request.getContextPath()%>/vistas/js/todosVehiculos.jsp">Todos los automoviles</a>
                </li>
                <li>
                    <a href="<%=request.getContextPath()%>/vistas/js/agregarVehiculo.jsp">Agregar automovil</a>
                </li>
                <li>
                    <a href="<%=request.getContextPath()%>/vistas/js/buscarVehiculo.jsp">Buscar automovil</a>
                </li>
              </ul>
	          </li>
	        </ul>
	        <div class="footer">
	        	<p>Para m�s informaci�n contactar a los miembros del equipo:</p>
	        	<p>Trujillo Carcamo Diego Alexis</p>
	        	<p>Rojas Flores Luis Fernando</p>
	        </div>
	      </div>
    	</nav>
        <!-- Page Content  -->
      <div id="content" class="p-4 p-md-5">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <div class="container-fluid">
            <button type="button" id="sidebarCollapse" class="btn btn-primary">
              <i class="fa fa-bars"></i>
              <span class="sr-only">Toggle Menu</span>
            </button>
            <button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <i class="fa fa-bars"></i>
            </button>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="nav navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="#">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Ingresar</a>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <h2 class="mb-4">Administrador de residencias</h2>
        <p>Esta p�gina fue desarrollada en el programa de Semilleros por el equipo de JAVA </p>
        <p>Trujillo Carcamo Diego Alexis.</p>
        <p>Rojas Flores LuisFernando</p>
      </div>
		</div>
    <script src="<%=request.getContextPath()%>/vistas/js/jquery.min.js"></script>
    <script src="<%=request.getContextPath()%>/vistas/js/popper.js"></script>
    <script src="<%=request.getContextPath()%>/vistas/js/bootstrap.min.js"></script>
    <script src="<%=request.getContextPath()%>/vistas/js/main.js"></script>
  </body>
</html>