<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/vistas/css/loginCSS.css">
</head>
<body>

	<div class="container register">
		<div class="row">
			<div class="col-md-3 register-left">
				<img src="" alt="" />
				<h3></h3>
				<p></p>
			</div>
			<div class="col-md-9 register-right">
				<ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
					<li class="nav-item"><a class="nav-link active" id="login-tab"
						data-toggle="tab"
						href="<%=request.getContextPath()%>/vistas/js/buscaPropietario.jsp"
						role="tab" aria-controls="Propietario" aria-selected="false">Propietario</a>
					</li>
					<li class="nav-item"><a class="nav-link" id="newuser-tab"
						data-toggle="tab"
						href="<%=request.getContextPath()%>/vistas/js/buscaArrendatario.jsp"
						role="tab" aria-controls="Arrendatario" aria-selected="true">Arrendatario</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="login" role="tabpanel"
						aria-labelledby="login-tab">
						<h3 class="register-heading">Busca Propietario</h3>
						<div class="row register-form">
							<div class="col-md-12 profile_card">
								<form id="login-form" class="form-inline"
									action="<%=request.getContextPath()%>/BuscaPropietario"
									method="post">
									<div class="form-group">
										<i class="fa fa-lock"></i> <input type="text"
											class="form-control" placeholder="CURP" name="curp" value=""
											required />
									</div>
									<div class="float-right">
										<input type="submit" class="btn btn-primary" value="Buscar" />
									</div>
									<div class="float-right">
										<input type="button" class="btn btn-primary" value="Regresar" onclick="location.href='<%=request.getContextPath() %>/vistas/inicio.jsp';" />
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</body>
</html>