<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Agregar Vehiculo</title>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/vistas/css/loginCSS.css">
</head>
<body>

	<div class="container register">
		<div class="row">
			<div class="col-md-3 register-left">
				<img src="" alt="" />
				<h3></h3>
				<p></p>
			</div>
			<div class="col-md-9 register-right">
				
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="login" role="tabpanel"
						aria-labelledby="login-tab">
						<h3 class="register-heading">Buscar Vehiculo</h3>
						<div class="row register-form">
							<div class="col-md-12 profile_card">
								<form id="login-form" class="form-inline"
									action="<%=request.getContextPath()%>/BuscarVehiculo"
									method="post">
									<div class="form-group">
										<i class="fa fa-envelope-o"></i> <input type="text"
											class="form-control" placeholder="Placas" name="_id" value=""
											required>
									</div>
									<div class="float-right">
										<input type="submit"  class="btn btn-primary" value="Buscar" style="background-color: #4CAF50; border-color:#4CAF50;"/>
									</div>
									<div class="float-right">
										<input type="button" class="btn btn-primary" value="Regresar" onclick="location.href='<%=request.getContextPath() %>/vistas/inicio.jsp';" />
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>




</body>
</html>