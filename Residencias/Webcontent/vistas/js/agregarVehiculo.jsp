<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.develop.modelos.PropietariosM"%>
<%@ page import="com.develop.modelos.ArrendatarioM"%>
<%@ page import="com.develop.DAO.PropietarioDAO"%>
<%@ page import="com.develop.DAO.ArrendatarioDAO"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Agregar Vehiculo</title>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/vistas/css/loginCSS.css">
</head>
<body>

	<div class="container register">
		<div class="row">
			<div class="col-md-3 register-left">
				<img src="" alt="" />
				<h3></h3>
				<p></p>
			</div>
			<div class="col-md-9 register-right">
				
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="login" role="tabpanel"
						aria-labelledby="login-tab">
						<h3 class="register-heading">Agrega Vehiculo</h3>
						<div class="row register-form">
							<div class="col-md-12 profile_card">
								<form id="login-form" 
									action="<%=request.getContextPath()%>/AgregarVehiculo"
									method="post">
									<div class="form-group">
										<i class="fa fa-envelope-o"></i> <input type="text"
											class="form-control" placeholder="Placas" name="_id" value=""
											required>
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i> <input type="text"
											class="form-control" placeholder="Marca" name="marca"
											value="" required />
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i> <input type="text"
											class="form-control" placeholder="Modelo" name="modelo"
											value="" required />
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i> <input type="text"
											class="form-control" placeholder="Color" name="color"
											value="" required />
									</div>
									<%PropietarioDAO dao = new PropietarioDAO();
										ArrayList<PropietariosM> propietarios =dao.listaPropietario();
										ArrendatarioDAO dao2 = new ArrendatarioDAO();
										ArrayList<ArrendatarioM> arrendatarios =dao2.ListaArrendatario();
										%>
									<div class="form-group">
									<label for="users">Usuarios registrados:</label>
										<i class="fa fa-lock"></i><select style="width:210px" name="usuarios">
											<%for(int i =0;i<propietarios.size();i++){ %>
											<option><%=propietarios.get(i).getCurpPropietario()%></option>
											<%
												
											}%>
											
											<%for(int i =0;i<arrendatarios.size();i++){ %>
											<option><%=arrendatarios.get(i).getCurpArrendatario()%></option>
											<%
												
											}%>
										</select>
									</div>
									<div class="float-right">
										<input style="background-color: #4CAF50; border-color:#4CAF50; " type="submit" class="btn btn-primary" value="Registrar" />
									</div>
									<div class="float-right">
										<input type="button" class="btn btn-primary" value="Regresar" onclick="location.href='<%=request.getContextPath() %>/vistas/inicio.jsp';" />
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>




</body>
</html>