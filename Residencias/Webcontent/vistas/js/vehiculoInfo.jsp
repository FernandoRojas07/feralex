<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Información de vehiculo</title>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/vistas/css/loginCSS.css">
</head>
<body>

	<div class="container register">
		<div class="row">
			<div class="col-md-3 register-left">
				<img src="" alt="" />
				<h3></h3>
				<p></p>
			</div>
			<div class="col-md-9 register-right" >
				
				<div class="tab-content" id="myTabContent"style="padding-left: 35px;">
					<div class="tab-pane fade show active" id="login" role="tabpanel"
						aria-labelledby="login-tab">
						<h3 class="register-heading">Información de Vehiculo</h3>
						<div class="row register-form">
							<div class="col-md-12 profile_card">
								<form id="login-form" class="form-inline">
									<div class="form-group">
										<i class="fa fa-envelope-o"></i>Placas <input type="text"
											class="form-control" name="_id"
											value="<%=request.getAttribute("_id")%>" readonly>
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i>Marca<input type="text"
											class="form-control" name="marca"
											value="<%=request.getAttribute("marca")%>" readonly />
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i>Modelo<input type="text"
											class="form-control" name="modelo"
											value="<%=request.getAttribute("modelo")%>" readonly />
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i>Color<input type="text"
											class="form-control" name="color"
											value="<%=request.getAttribute("color")%>" readonly />
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i>Propietario<input type="text"
											class="form-control" name="propietario"
											value="<%=request.getAttribute("propietario")%>" readonly />
									</div>
									

									<div class="float-right">
										<input type="button" class="btn btn-primary" value="Regresar" onclick="location.href='<%=request.getContextPath() %>/vistas/js/buscarVehiculo.jsp';"/>
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

</body>
</html>