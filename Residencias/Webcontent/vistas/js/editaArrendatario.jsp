<%@page import="java.util.Date"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/vistas/css/loginCSS.css">
</head>
<body>
<div class="container register">
		<div class="row">
			<div class="col-md-3 register-left">
				<img src="" alt="" />
				<h3></h3>
				<p></p>
			</div>
			<div class="col-md-9 register-right">
				<ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
					<li class="nav-item"><a class="nav-link " id="login-tab"
						data-toggle="tab"
						href="<%=request.getContextPath()%>/vistas/js/agregaPropietario.jsp"
						role="tab" aria-controls="Propietario" aria-selected="true">Propietario</a>
					</li>
					<li class="nav-item"><a class="nav-link active"
						id="newuser-tab" data-toggle="tab"
						href="<%=request.getContextPath()%>/vistas/js/agregaArrendatario.jsp"
						role="tab" aria-controls="Arrendatario" aria-selected="false">Arrendatario</a>
					</li>
				</ul>
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="login" role="tabpanel"
						aria-labelledby="login-tab">
						<h3 class="register-heading">Editar Arrendatario</h3>
						<div class="row register-form">
							<div class="col-md-12 profile_card">
								<form id="login-form" class="form-inline"
									action="<%=request.getContextPath()%>/EditaArrendatario"
									method="post">
									<div class="form-group">
										<i class="fa fa-envelope-o"></i> <input type="text"
											class="form-control" name="nombreArrendatario"
											value="<%=request.getParameter("nombre")%>" required>
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i> <input type="text"
											class="form-control" name="apPaterno"
											value="<%=request.getParameter("apPaterno")%>" required/>
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i> <input type="text"
											class="form-control" name="apMaterno"
											value="<%=request.getParameter("apMaterno")%>" required />
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i> <input type="text"
											class="form-control" name="curp"
											value="<%=request.getParameter("curp")%>" required />
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i> <input type="text"
											class="form-control" name="telefono"
											value="<%=request.getParameter("telefono")%>" required />
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i> <input type="text"
											class="form-control" name="edad"
											value="<%=request.getParameter("edad")%>" required />
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i> <input type="text"
											class="form-control" name="fecheInicio"
											value="<%=request.getParameter("fhInicio")%>" required />
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i> <input type="text"
											class="form-control" name="fechaFin"
											value="<%=request.getParameter("fhFin")%>" required />
									</div>

									<div class="float-right">
										<input type="submit" class="btn btn-primary" value="Editar" />
									</div>
								</form>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>

</body>
</html>