<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Iterator"%>
<%@ page import="com.develop.modelos.PropietariosM"%>
<%@ page import="com.develop.modelos.ArrendatarioM"%>
<%@ page import="com.develop.DAO.PropietarioDAO"%>
<%@ page import="com.develop.DAO.ArrendatarioDAO"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registro Propiedad</title>
<link
	href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css"
	rel="stylesheet" id="bootstrap-css">
<script
	src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script
	src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="<%=request.getContextPath()%>/vistas/css/loginCSS.css">
</head>
<body>

	<div class="container register">
		<div class="row">
			<div class="col-md-3 register-left">
				<img src="" alt="" />
				<h3></h3>
				<p></p>
			</div>
			<div class="col-md-9 register-right">
				
				<div class="tab-content" id="myTabContent">
					<div class="tab-pane fade show active" id="login" role="tabpanel"
						aria-labelledby="login-tab">
						<h3 class="register-heading">Agrega Propiedad</h3>
						<div class="row register-form">
							<div class="col-md-12 profile_card">
								<form id="login-form" class="form-inline"
									action="<%=request.getContextPath()%>/AgregaPropietario"
									method="post">
									<div class="form-group">
										<i class="fa fa-envelope-o"></i> <input type="text"
											class="form-control" placeholder="Calle"
											name="callePropiedad" value="" required>
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i> <input type="text"
											class="form-control" placeholder="Numero"
											name="numeroPropiedad" value="" required />
									</div>
									
									<div class="form-group">
										<label for="users">Propietarios registrados:</label>
										<i class="fa fa-lock"></i><select style="width:210px" name="propietarios">
										
										<%
										PropietarioDAO dao = new PropietarioDAO();
										ArrayList<PropietariosM> propietarios =dao.listaPropietario();
										ArrendatarioDAO dao2 = new ArrendatarioDAO();
										ArrayList<ArrendatarioM> arrendatarios =dao2.ListaArrendatario();
										%>
										<%
											for(int i =0;i<propietarios.size();i++){ %>
											<option><%=propietarios.get(i).getCurpPropietario()%></option>
											<%
												
											}%>
										</select>
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i> <input type="text"
											class="form-control" placeholder="Caseta" name="caseta" value=""
											required />
									</div>
									<div class="form-group">
										<i class="fa fa-lock"></i><select style="width:210px" name="arrendatarios">
										<%for(int i =0;i<arrendatarios.size();i++){ %>
											<option><%=arrendatarios.get(i).getCurpArrendatario()%></option>
											<%
												
											}%> 
										</select>
									</div>
									
									<div class="form-group">
										<i class="fa fa-lock"></i> 
										<input type="radio" id="contactChoice1" name="venta" value="Renta">
									</div>
									
									<div class="form-group">
										<i class="fa fa-lock"></i>
										<input type="radio" id="contactChoice1" name="renta" value="Renta"> 
									</div>

									<div class="float-right">
										<input type="submit" class="btn btn-primary" value="Registrar" />
									</div>
									<div class="float-right">
										<input type="button" class="btn btn-primary" value="Regresar" onclick="location.href='<%=request.getContextPath() %>/vistas/inicio.jsp';" />
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


</body>
</html>