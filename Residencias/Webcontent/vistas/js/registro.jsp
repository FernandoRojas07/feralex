<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Registro</title>
<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath()%>/vistas/css/loginCSS.css">
</head>
<body>
		<div class="container register">
		<div class="row">
		<div class="col-md-3 register-left">
		<img src="" alt="" />
		<h3></h3>
		<p></p>
		</div>
		<div class="col-md-9 register-right">
		<ul class="nav nav-tabs nav-justified" id="myTab" role="tablist">
		<li class="nav-item">
		<a class="nav-link" id="login-tab" data-toggle="tab" href="<%=request.getContextPath()%>/vistas/js/Login.jsp" role="tab" aria-controls="login" aria-selected="false">Login</a>
		</li>
		<li class="nav-item">
		<a class="nav-link active" id="newuser-tab" data-toggle="tab" href="<%=request.getContextPath()%>/vistas/js/registro.jsp" role="tab" aria-controls="newuser" aria-selected="true">New User</a>
		</li>
		</ul>
		<div class="tab-content" id="myTabContent">
		<div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
		<h3 class="register-heading">Regístrate</h3>
		<div class="row register-form">
		<div class="col-md-12 profile_card">
			<form id="login-form" class="form-inline" action="<%=request.getContextPath()%>/Registro" method="post">
			    <div class="form-group">
			        <i class="fa fa-envelope-o"></i>
			        <input type="text" class="form-control" placeholder="Usuario" value="" required/>
			    </div>
			    <div class="form-group">
			             <i class="fa fa-lock"></i>
			        <input type="password" class="form-control" placeholder="Contraseña *" value="" required/>
			    </div>
			    <div class="float-right">
			      <input type="submit" class="btn btn-primary" value="Registrarse" />
			      </div>
		    </form>
		</div>
		</div>
		</div>
		<div class="tab-pane fade show" id="newuser" role="tabpanel" aria-labelledby="newuser-tab">
		<h3 class="register-heading">New User</h3>
		<div class="row register-form">
		<div class="col-md-6">
		    <div class="form-group">
		        <input type="text" class="form-control" placeholder="First Name *" value="" />
		    </div>
		    <div class="form-group">
		        <input type="text" class="form-control" placeholder="Last Name *" value="" />
		    </div>
		    <div class="form-group">
		        <input type="email" class="form-control" placeholder="Email *" value="" />
		    </div>
		    <div class="form-group">
		        <input type="text" maxlength="10" minlength="10" class="form-control" placeholder="Phone *" value="" />
		    </div>
		
		</div>
		<div class="col-md-6">
		    <div class="form-group">
		        <input type="password" class="form-control" placeholder="Password *" value="" />
		    </div>
		    <div class="form-group">
		        <input type="password" class="form-control" placeholder="Confirm Password *" value="" />
		    </div>
		     <div class="float-right">
		    <input type="submit" class="btn btn-primary" value="Register" />
		    </div>
		</div>
		</div>
		</div>
		</div>
		</div>
		</div>
		</div>
</body>
</html>