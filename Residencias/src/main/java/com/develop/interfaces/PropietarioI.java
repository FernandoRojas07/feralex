package com.develop.interfaces;

import java.util.ArrayList;
import com.develop.modelos.PropietariosM;

public interface PropietarioI {

	public String agregaPropietario(PropietariosM propietario);
	
	public String editaPropietario(PropietariosM propietario);
	
	public String eliminaPropietario(String curp);
	
	public PropietariosM buscaPropietario(String curp);
	
	public ArrayList<PropietariosM> listaPropietario();
	
}
