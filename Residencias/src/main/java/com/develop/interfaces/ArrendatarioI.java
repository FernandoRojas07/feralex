package com.develop.interfaces;

import java.util.ArrayList;
import com.develop.modelos.ArrendatarioM;

public interface ArrendatarioI {

	public String agregaArrendatario(ArrendatarioM arrendatario);
	
	public String eliminaArrendatario(String curp);
	
	public String editaArrendatarios(ArrendatarioM arrendatario);
	
	public ArrendatarioM buscaArrendatario(String curp);
	
	public ArrayList<ArrendatarioM> ListaArrendatario();
	
}
