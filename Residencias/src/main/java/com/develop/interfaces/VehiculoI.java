package com.develop.interfaces;

import java.util.ArrayList;

import com.develop.modelos.VehiculoM;

public interface VehiculoI {
	public String agregarVehiculo(VehiculoM v);
	public String eliminarVehiculo(VehiculoM v);
	public String editarVehiculo(VehiculoM v);
	public VehiculoM buscarVehiculo(String placas);
	public ArrayList<VehiculoM> listVehiculos();
}
