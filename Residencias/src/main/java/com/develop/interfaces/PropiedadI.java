package com.develop.interfaces;

import java.util.ArrayList;

import com.develop.modelos.PropiedadesM;


public interface PropiedadI {
	
	public String agregaPropiedad(PropiedadesM propiedad);
	
	public String editaPropiedad(PropiedadesM propiedad);
	
	public String eliminaPropiedad(String curp);
	
	public PropiedadesM buscaPropiedad(String curp);
	
	public ArrayList<PropiedadesM> listaPropietario();
}
