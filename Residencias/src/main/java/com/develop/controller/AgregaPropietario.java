package com.develop.controller;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.develop.DAO.PropietarioDAO;
import com.develop.modelos.PropietariosM;

/**
 * Servlet implementation class AgregaPropietario
 */
@WebServlet("/AgregaPropietario")
public class AgregaPropietario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AgregaPropietario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PropietariosM propietario = new PropietariosM();
		PropietarioDAO propiDAO = new PropietarioDAO();
		String resp = "";
		RequestDispatcher rq;
		propietario.setCurpPropietario(request.getParameter("curp"));
		propietario.setNombrePropietario(request.getParameter("nombrePropietario"));
		propietario.setApPatPropietario(request.getParameter("apPaterno"));
		propietario.setApMatPropietario(request.getParameter("apMaterno"));
		propietario.setEdadPropiertio(Integer.parseInt(request.getParameter("edad")));
		propietario.setTelefonoPropierario(request.getParameter("telefono"));
		resp = propiDAO.agregaPropietario(propietario);
		if(resp == "OK") {
			System.out.println("Se insert� de forma exitosa");
			rq = request.getRequestDispatcher("/vistas/js/main.jsp");
			rq.forward(request, response);
		}else{
			System.out.println("No se puro insertar");
			rq = request.getRequestDispatcher("/vistas/js/agregaPropietario.jsp");
			rq.forward(request, response);
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
