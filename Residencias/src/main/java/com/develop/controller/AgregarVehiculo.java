package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.modelos.VehiculoM;
import com.develop.DAO.VehiculoDAO;


/**
 * Servlet implementation class agregarVehiculo
 */
@WebServlet("/AgregarVehiculo")
public class AgregarVehiculo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AgregarVehiculo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		VehiculoM vehiculo = new VehiculoM();
		vehiculo.setIdPlacas(request.getParameter("_id"));
		vehiculo.setMarcaVehiculo(request.getParameter("marca"));
		vehiculo.setModelo(request.getParameter("modelo"));
		vehiculo.setColorVehiculo(request.getParameter("color"));
		String s=request.getParameter("usuarios");
		System.out.println(s);
		vehiculo.setPropietario(s);
		
		VehiculoDAO vh = new VehiculoDAO();
		vh.agregarVehiculo(vehiculo);
		RequestDispatcher rq;
		rq = request.getRequestDispatcher("vistas/js/todosVehiculos.jsp");
		rq.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
