package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.ArrendatarioDAO;
import com.develop.modelos.ArrendatarioM;

/**
 * Servlet implementation class BuscaArrendatario
 */
@WebServlet("/BuscaArrendatario")
public class BuscaArrendatario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuscaArrendatario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		System.out.println("Hola desde el servlet de b�squeda");
		ArrendatarioDAO arrenDAO = new ArrendatarioDAO();
		String curp = request.getParameter("curp");
		ArrendatarioM arrendatario;
		RequestDispatcher rq;
		arrendatario = arrenDAO.buscaArrendatario(curp);
		if(!arrendatario.equals(null)) {
			request.setAttribute("nombre", arrendatario.getNombreArrendatario());
			request.setAttribute("apPaterno", arrendatario.getApPatArrendatario());
			request.setAttribute("apMaterno", arrendatario.getApMatArrendatario());
			request.setAttribute("curp", arrendatario.getCurpArrendatario());
			request.setAttribute("edad", arrendatario.getEdadArrendatario());
			request.setAttribute("telefono", arrendatario.getTelefonoArrendatario());
			request.setAttribute("fhInicio", arrendatario.getFechaInicio());
			request.setAttribute("fhFin", arrendatario.getFechaFin());
			rq = request.getRequestDispatcher("vistas/js/arrenInfo.jsp");
			rq.forward(request, response);
		}else {
			rq = request.getRequestDispatcher("vistas/js/buscaArrendatario.jsp");
			rq.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
