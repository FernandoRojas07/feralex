package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.PropietarioDAO;
import com.develop.modelos.PropietariosM;

/**
 * Servlet implementation class BuscaPropietario
 */
@WebServlet("/BuscaPropietario")
public class BuscaPropietario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuscaPropietario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PropietarioDAO propiDAO = new PropietarioDAO();
		PropietariosM propietario;
		String curp = request.getParameter("curp");
		RequestDispatcher rq;
		propietario = propiDAO.buscaPropietario(curp);
		if(!propietario.equals(null)) {
			request.setAttribute("nombre", propietario.getNombrePropietario());
			request.setAttribute("apPaterno", propietario.getApPatPropietario());
			request.setAttribute("apMaterno", propietario.getApMatPropietario());
			request.setAttribute("curp", propietario.getCurpPropietario());
			request.setAttribute("edad", propietario.getEdadPropiertio());
			request.setAttribute("telefono", propietario.getTelefonoPropierario());
			rq = request.getRequestDispatcher("vistas/js/propiInfo.jsp");
			rq.forward(request, response);
		}else {
			rq = request.getRequestDispatcher("vistas/js/buscaPropietario.jsp");
			rq.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
