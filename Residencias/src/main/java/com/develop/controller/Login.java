package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public Login() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDispatcher rq;
		String usuario1 = "Fer";
		String usuario2 = "Alexis";
		String password = "1234";
		String user = request.getParameter("usuario");
		String pass = request.getParameter("contrase�a");
		if((user.equals(usuario1) || user.equals(usuario2)) && pass.equals(password)){
			System.out.println("Usuario v�lido");
			rq = request.getRequestDispatcher("/vistas/inicio.jsp");
			rq.forward(request, response);
		}else {
			System.out.println("Usuario no v�lido");
			rq = request.getRequestDispatcher("/index.jsp");
			rq.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
