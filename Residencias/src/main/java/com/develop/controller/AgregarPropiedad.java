package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.PropiedadDAO;
import com.develop.DAO.PropietarioDAO;
import com.develop.modelos.PropiedadesM;
import com.develop.modelos.PropietariosM;

/**
 * Servlet implementation class AgregarPropiedad
 */
@WebServlet("/AgregarPropiedad")
public class AgregarPropiedad extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public AgregarPropiedad() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		PropiedadesM propiedad = new PropiedadesM();
		PropiedadDAO propiDAO = new PropiedadDAO();
		
		String resp = "";
		RequestDispatcher rq;
		
		PropietarioDAO propietarioDAO = new PropietarioDAO();
		PropietariosM propietario = propietarioDAO.buscaPropietario(request.getParameter("propietarios"));
		
		propiedad.setCalle(request.getParameter("callePropiedad"));
		propiedad.setNumero(request.getParameter("Numero"));
		propiedad.setPropietario(propietario);
		propiedad.setCaseta(request.getParameter("caseta"));
		propiedad.setArrendatarios(Integer.parseInt(request.getParameter("edad")));
		propiedad.setRenta(request.getParameter("telefono"));
		propiedad.setVenta(request.getParameter("telefono"));
		resp = propiDAO.agregaPropietario(propietario);
		if(resp == "OK") {
			System.out.println("Se insert� de forma exitosa");
			rq = request.getRequestDispatcher("/vistas/js/main.jsp");
			rq.forward(request, response);
		}else{
			System.out.println("No se puro insertar");
			rq = request.getRequestDispatcher("/vistas/js/agregaPropietario.jsp");
			rq.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
