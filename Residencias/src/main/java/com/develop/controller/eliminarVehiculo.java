package com.develop.controller;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.develop.DAO.VehiculoDAO;
import com.develop.modelos.VehiculoM;

/**
 * Servlet implementation class eliminarVehiculo
 */
@WebServlet("/eliminarVehiculo")
public class eliminarVehiculo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public eliminarVehiculo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		VehiculoDAO vDao = new VehiculoDAO();
		VehiculoM vehiculo = new VehiculoM();
		String placas=request.getParameter("_id");
		vehiculo.setIdPlacas(placas);
		String respuesta = vDao.eliminarVehiculo(vehiculo);
		
		request.setAttribute("resultado", respuesta + " placas: "+placas);
		request.getRequestDispatcher("/vistas/js/agregarVehiculo.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
