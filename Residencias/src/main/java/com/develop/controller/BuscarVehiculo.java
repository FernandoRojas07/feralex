package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.DAO.VehiculoDAO;
import com.develop.modelos.VehiculoM;

/**
 * Servlet implementation class BuscarVehiculo
 */
@WebServlet("/BuscarVehiculo")
public class BuscarVehiculo extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuscarVehiculo() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("entr� en buscar vehiculo");
		VehiculoDAO vehiDAO = new VehiculoDAO();
		VehiculoM vehiculo;
		
		RequestDispatcher rq;
		vehiculo = vehiDAO.buscarVehiculo(request.getParameter("_id"));
		
		if(vehiculo == null) {
			rq = request.getRequestDispatcher("vistas/js/buscarVehiculo.jsp");
			rq.forward(request, response);
		}else {
			request.setAttribute("_id", vehiculo.getIdPlacas());
			request.setAttribute("marca", vehiculo.getMarcaVehiculo());
			request.setAttribute("modelo", vehiculo.getModelo());
			request.setAttribute("color", vehiculo.getColorVehiculo());
			request.setAttribute("propietario", vehiculo.getPropietario());
			rq = request.getRequestDispatcher("vistas/js/vehiculoInfo.jsp");
			rq.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
