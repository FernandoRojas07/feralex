package com.develop.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.develop.modelos.ArrendatarioM;
import com.develop.DAO.ArrendatarioDAO;

/**
 * Servlet implementation class AgregaArrendatario
 */
@WebServlet("/AgregaArrendatario")
public class AgregaArrendatario extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AgregaArrendatario() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		ArrendatarioM arrendatario = new ArrendatarioM();
		ArrendatarioDAO arrenDAO = new ArrendatarioDAO();
		String resp = "";
		RequestDispatcher rq;
		arrendatario.setCurpArrendatario(request.getParameter("curp"));
		arrendatario.setNombreArrendatario(request.getParameter("nombreArrendatario"));
		arrendatario.setApPatArrendatario(request.getParameter("apPaterno"));
		arrendatario.setApMatArrendatario(request.getParameter("apMaterno"));
		arrendatario.setTelefonoArrendatario(request.getParameter("telefono"));
		arrendatario.setEdadArrendatario(Integer.parseInt(request.getParameter("edad")));
		arrendatario.setFechaInicio(request.getParameter("fecheInicio"));
		arrendatario.setFechaFin(request.getParameter("fechaFin"));
		resp = arrenDAO.agregaArrendatario(arrendatario);
		if(resp == "OK") {
			System.out.println("Se insert� de forma exitosa");
			rq = request.getRequestDispatcher("/vistas/js/main.jsp");
			rq.forward(request, response);
		}else{
			System.out.println("No se pudo insertar");
			rq = request.getRequestDispatcher("/vistas/js/agregaArrendatario.jsp");
			rq.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
