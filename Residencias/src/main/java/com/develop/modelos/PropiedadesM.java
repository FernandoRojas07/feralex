package com.develop.modelos;

import java.util.ArrayList;

public class PropiedadesM {
	private Long _id;
	private String calle;
	private String numero;
	private PropietariosM propietario;
	private String caseta;
	private ArrendatarioM arrendatarios;
	private boolean renta;
	private boolean venta;
	
	public PropiedadesM() {
		super();
	}

	public PropiedadesM(String calle, String numero, PropietariosM propietario, String caseta,
			ArrendatarioM arrendatarios, boolean renta, boolean venta) {
		super();
		this.calle = calle;
		this.numero = numero;
		this.propietario = propietario;
		this.caseta = caseta;
		this.arrendatarios = arrendatarios;
		this.renta = renta;
		this.venta = venta;
	}

	public Long get_id() {
		return _id;
	}

	public void set_id(Long _id) {
		this._id = _id;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public PropietariosM getPropietario() {
		return propietario;
	}

	public void setPropietario(PropietariosM propietario) {
		this.propietario = propietario;
	}

	public String getCaseta() {
		return caseta;
	}

	public void setCaseta(String caseta) {
		this.caseta = caseta;
	}

	public boolean isRenta() {
		return renta;
	}

	public void setRenta(boolean renta) {
		this.renta = renta;
	}

	public boolean isVenta() {
		return venta;
	}

	public void setVenta(boolean venta) {
		this.venta = venta;
	}

	public ArrendatarioM getArrendatarios() {
		return arrendatarios;
	}

	public void setArrendatarios(ArrendatarioM arrendatarios) {
		this.arrendatarios = arrendatarios;
	}
	
	
	
}
