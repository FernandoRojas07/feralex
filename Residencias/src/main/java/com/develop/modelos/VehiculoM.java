package com.develop.modelos;

public class VehiculoM {
	
	private String idPlacas;
	private String marcaVehiculo;
	private String colorVehiculo;
	private String modelo;
	private String propietario;
	
	public VehiculoM() {
		super();
	}

	public VehiculoM(String idPlacas, String marcaVehiculo, String colorVehiculo,String modelo,String propietario) {
		super();
		this.idPlacas = idPlacas;
		this.marcaVehiculo = marcaVehiculo;
		this.colorVehiculo = colorVehiculo;
		this.modelo = modelo;
		this.propietario = propietario;
	}

	public String getIdPlacas() {
		return idPlacas;
	}

	public void setIdPlacas(String idPlacas) {
		this.idPlacas = idPlacas;
	}

	public String getMarcaVehiculo() {
		return marcaVehiculo;
	}

	public void setMarcaVehiculo(String marcaVehiculo) {
		this.marcaVehiculo = marcaVehiculo;
	}

	public String getColorVehiculo() {
		return colorVehiculo;
	}

	public void setColorVehiculo(String colorVehiculo) {
		this.colorVehiculo = colorVehiculo;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public String getPropietario() {
		return propietario;
	}

	public void setPropietario(String s) {
		this.propietario=s;
		
	}
	
}
