package com.develop.modelos;

public class PropietariosM {
	
	private String curpPropietario;
	private String nombrePropietario;
	private String apPatPropietario;
	private String apMatPropietario;
	private String telefonoPropierario;
	private int edadPropiertio;

	public PropietariosM() {
		super();
	}

	public PropietariosM(String curpPropietario, String nombrePropietario, String apPatPropietario,
			String apMatPropietario, String telefonoPropierario, int edadPropiertio, VehiculoM vehiculo) {
		super();
		this.curpPropietario = curpPropietario;
		this.nombrePropietario = nombrePropietario;
		this.apPatPropietario = apPatPropietario;
		this.apMatPropietario = apMatPropietario;
		this.telefonoPropierario = telefonoPropierario;
		this.edadPropiertio = edadPropiertio;
	}

	public String getCurpPropietario() {
		return curpPropietario;
	}

	public void setCurpPropietario(String curpPropietario) {
		this.curpPropietario = curpPropietario;
	}

	public String getNombrePropietario() {
		return nombrePropietario;
	}

	public void setNombrePropietario(String nombrePropietario) {
		this.nombrePropietario = nombrePropietario;
	}

	public String getApPatPropietario() {
		return apPatPropietario;
	}

	public void setApPatPropietario(String apPatPropietario) {
		this.apPatPropietario = apPatPropietario;
	}

	public String getApMatPropietario() {
		return apMatPropietario;
	}

	public void setApMatPropietario(String apMatPropietario) {
		this.apMatPropietario = apMatPropietario;
	}

	public String getTelefonoPropierario() {
		return telefonoPropierario;
	}

	public void setTelefonoPropierario(String telefonoPropierario) {
		this.telefonoPropierario = telefonoPropierario;
	}

	public int getEdadPropiertio() {
		return edadPropiertio;
	}

	public void setEdadPropiertio(int edadPropiertio) {
		this.edadPropiertio = edadPropiertio;
	}

}
