package com.develop.modelos;

public class ArrendatarioM {

	private String curpArrendatario;
	private String nombreArrendatario;
	private String apPatArrendatario;
	private String apMatArrendatario;
	private String telefonoArrendatario;
	private int edadArrendatario;
	private String fechaInicio;
	private String fechaFin;
	
	public ArrendatarioM() {
		super();
	}

	
	public ArrendatarioM(String curpArrendatario, String nombreArrendatario, String apPatArrendatario,
			String apMatArrendatario, String telefonoArrendatario, int edadArrendatario, String fechaInicio,
			String fechaFin) {
		super();
		this.curpArrendatario = curpArrendatario;
		this.nombreArrendatario = nombreArrendatario;
		this.apPatArrendatario = apPatArrendatario;
		this.apMatArrendatario = apMatArrendatario;
		this.telefonoArrendatario = telefonoArrendatario;
		this.edadArrendatario = edadArrendatario;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
	}


	public String getCurpArrendatario() {
		return curpArrendatario;
	}

	public void setCurpArrendatario(String curpArrendatario) {
		this.curpArrendatario = curpArrendatario;
	}

	public String getNombreArrendatario() {
		return nombreArrendatario;
	}

	public void setNombreArrendatario(String nombreArrendatario) {
		this.nombreArrendatario = nombreArrendatario;
	}

	public String getApPatArrendatario() {
		return apPatArrendatario;
	}

	public void setApPatArrendatario(String apPatArrendatario) {
		this.apPatArrendatario = apPatArrendatario;
	}

	public String getApMatArrendatario() {
		return apMatArrendatario;
	}

	public void setApMatArrendatario(String apMatArrendatario) {
		this.apMatArrendatario = apMatArrendatario;
	}

	public String getTelefonoArrendatario() {
		return telefonoArrendatario;
	}

	public void setTelefonoArrendatario(String telefonoArrendatario) {
		this.telefonoArrendatario = telefonoArrendatario;
	}

	public int getEdadArrendatario() {
		return edadArrendatario;
	}

	public void setEdadArrendatario(int edadArrendatario) {
		this.edadArrendatario = edadArrendatario;
	}

	public String getFechaInicio() {
		return fechaInicio;
	}


	public void setFechaInicio(String fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public String getFechaFin() {
		return fechaFin;
	}


	public void setFechaFin(String fechaFin) {
		this.fechaFin = fechaFin;
	}
	
}
