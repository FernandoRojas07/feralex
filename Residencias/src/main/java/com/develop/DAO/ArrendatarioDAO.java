package com.develop.DAO;

import java.util.ArrayList;
import org.bson.Document;
import com.develop.modelos.ArrendatarioM;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.develop.config.MongoDBConexion;
import com.develop.interfaces.ArrendatarioI;

public class ArrendatarioDAO implements ArrendatarioI{
	
	MongoClient mongoClient = new MongoDBConexion().getClient();

	@Override
	public String agregaArrendatario(ArrendatarioM arrendatario) {
		MongoDatabase database = mongoClient.getDatabase("Residencias");
		MongoCollection<Document> collection = database.getCollection("Arrendatarios");
		Document d1 = new Document();
		String mensaje = "";
		try {
			d1.append("_id", arrendatario.getCurpArrendatario());
			d1.append("nombre", arrendatario.getNombreArrendatario());
			d1.append("apPaterno", arrendatario.getApPatArrendatario());
			d1.append("apMaterno", arrendatario.getApMatArrendatario());
			d1.append("edad", arrendatario.getEdadArrendatario());
			d1.append("telefono", arrendatario.getTelefonoArrendatario());
			d1.append("fechaInicio", arrendatario.getFechaInicio());
			d1.append("fechaFin", arrendatario.getFechaFin());
			collection.insertOne(d1);
			System.out.println("Arrendatario agregado exitosamente.");
			mensaje = "OK";
			}catch(Exception e) {
				System.out.println("No se pudo insertar valores:");
				System.out.println(e.getMessage());
				mensaje = "NOT";
			}
		return mensaje;
	}

	@Override
	public String eliminaArrendatario(String curp) {
		MongoDatabase database = mongoClient.getDatabase("Residencias");
		MongoCollection<Document> collection = database.getCollection("Arrendatarios");
		String mensaje = "";
		try {
			collection.deleteOne(new Document("_id",curp));
			System.out.println("Arrendatario eliminado con exito.");
			mensaje = "OK";
		} catch (Exception e) {
			System.out.println("No se pudo eliminar: " + e.getMessage());
		}
		return mensaje;
	}

	@Override
	public String editaArrendatarios(ArrendatarioM arrendatario) {
		MongoDatabase database = mongoClient.getDatabase("Residencias");
		MongoCollection<Document> collection = database.getCollection("Arrendatarios");
		Document d1 = new Document();
		Document d2 = new Document();
		d2.append("_id", arrendatario.getCurpArrendatario());
		String mensaje = "";
		try {
			if(collection.countDocuments(d2)==0) {
				mensaje = "No existe en la base de datos";
			}else {
				d1.append("_id", arrendatario.getCurpArrendatario());
				d1.append("nombre", arrendatario.getNombreArrendatario());
				d1.append("apPaterno", arrendatario.getApPatArrendatario());
				d1.append("apMaterno", arrendatario.getApMatArrendatario());
				d1.append("edad", arrendatario.getEdadArrendatario());
				d1.append("telefono", arrendatario.getTelefonoArrendatario());
				d1.append("fechaInicio", arrendatario.getFechaInicio());
				d1.append("fechaFin", arrendatario.getFechaFin());
				collection.updateOne(d2, d1);
				System.out.println("Arrendatario actualizado exitosamente");
				mensaje = "OK";
			}
			return mensaje;
		}catch (Exception e) {
			System.out.println("No se pudo actualizar el arendatario");
			return mensaje;
		}
	}

	@Override
	public ArrendatarioM buscaArrendatario(String curp) {
		MongoDatabase database = mongoClient.getDatabase("Residencias");
		MongoCollection<Document> collection = database.getCollection("Arrendatarios");
		FindIterable<Document> iterable;
		ArrendatarioM arrendatario = new ArrendatarioM();
		try {
			iterable = collection.find(new Document("_id",curp));
			for(Document doc : iterable) {
			    arrendatario.setCurpArrendatario(doc.getString("_id"));
			    arrendatario.setNombreArrendatario(doc.getString("nombre"));
			    arrendatario.setApPatArrendatario(doc.getString("apPaterno"));
			    arrendatario.setApMatArrendatario(doc.getString("apMaterno"));
			    arrendatario.setTelefonoArrendatario(doc.getString("telefono"));
			    arrendatario.setEdadArrendatario(doc.getInteger("edad"));
			    arrendatario.setFechaInicio(doc.getString("fechaInicio"));
			    arrendatario.setFechaFin(doc.getString("fechaFin"));
			}
			return arrendatario;
		}catch (Exception e) {
			System.out.println("No se encontr� al arrendatario");
			return null;
		}
	}

	@Override
	public ArrayList<ArrendatarioM> ListaArrendatario() {
		MongoDatabase database = mongoClient.getDatabase("Residencias");
		MongoCollection<Document> collection = database.getCollection("Arrendatarios");
		ArrayList<ArrendatarioM> arrendatarios = new ArrayList<>();
		FindIterable<Document> docs;
		try {
			docs = collection.find();
			for(Document doc: docs) {
				ArrendatarioM arrendatario = new ArrendatarioM();
				arrendatario.setCurpArrendatario(doc.getString("_id"));
				arrendatario.setNombreArrendatario(doc.getString("nombre"));
			    arrendatario.setApPatArrendatario(doc.getString("apPaterno"));
			    arrendatario.setApMatArrendatario(doc.getString("apMaterno"));
			    arrendatario.setTelefonoArrendatario(doc.getString("telefono"));
			    arrendatario.setEdadArrendatario(doc.getInteger("edad"));
			    arrendatario.setFechaInicio(doc.getString("fechaInicio"));
			    arrendatario.setFechaFin(doc.getString("fechaFin"));
			    arrendatarios.add(arrendatario);
			}
			System.out.println("Lista conpletada con �xito");
			return arrendatarios;
		}catch (Exception e) {
			System.out.println("No se pudo completar");
			return null;
		}
	}
	
}
