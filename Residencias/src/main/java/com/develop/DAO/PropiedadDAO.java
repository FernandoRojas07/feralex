package com.develop.DAO;

import java.util.ArrayList;

import org.bson.Document;

import com.develop.config.MongoDBConexion;
import com.develop.interfaces.PropiedadI;
import com.develop.modelos.PropiedadesM;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class PropiedadDAO implements PropiedadI{
	MongoClient mongoClient = new MongoDBConexion().getClient();
	@Override
	public String agregaPropiedad(PropiedadesM propiedad) {
		try {
			MongoDatabase database = mongoClient.getDatabase("Residencias");
			MongoCollection<Document> collection = database.getCollection("Propiedades");
			
			Document d1 = new Document();
			
			d1.append("_id", propiedad.get_id());
			d1.append("calle", propiedad.getCalle());
			d1.append("numero", propiedad.getNumero());
			
			//Propietario
			Document d2 = new Document();
			d2.append("curp",propiedad.getPropietario().getCurpPropietario());
			d2.append("nombre",propiedad.getPropietario().getNombrePropietario());
			d2.append("apPaterno",propiedad.getPropietario().getApPatPropietario());
			d2.append("apMaterno",propiedad.getPropietario().getApMatPropietario());
			d2.append("telefono",propiedad.getPropietario().getTelefonoPropierario());
			d2.append("edad",propiedad.getPropietario().getEdadPropiertio());
			
			d1.append("propietario", d2);
			d1.append("caseta", propiedad.getCaseta());
			
			//Arrendatario
			Document d3 = new Document();
			d3.append("curp", propiedad.getArrendatarios().getCurpArrendatario());
			d3.append("nombre", propiedad.getArrendatarios().getNombreArrendatario());
			d3.append("apPaterno", propiedad.getArrendatarios().getApPatArrendatario());
			d3.append("apMaterno", propiedad.getArrendatarios().getApMatArrendatario());
			d3.append("telefono", propiedad.getArrendatarios().getTelefonoArrendatario());
			d3.append("edad", propiedad.getArrendatarios().getEdadArrendatario());
			d3.append("fechaInicio", propiedad.getArrendatarios().getFechaInicio());
			d3.append("fechaFin", propiedad.getArrendatarios().getFechaFin());
			
			
			d1.append("arrendatarios", d3);
			d1.append("renta", propiedad.isRenta());
			d1.append("venta", propiedad.isVenta());
			
			collection.insertOne(d1);
			System.out.println("Propiedad agregada exitosamente");
			return "Propiedad agregada exitosamente";
			
			}catch(Exception e) {
			
				System.out.println("No se pudo insertar valores:");
				System.out.println(e.getMessage());
				return "No se pudo acceder a la Base de datos";	
			}// TODO Auto-generated method stub
	}

	@Override
	public String editaPropiedad(PropiedadesM propiedad) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String eliminaPropiedad(String curp) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public PropiedadesM buscaPropiedad(String curp) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ArrayList<PropiedadesM> listaPropietario() {
		// TODO Auto-generated method stub
		return null;
	}
	

}
