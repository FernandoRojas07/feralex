package com.develop.DAO;

import java.util.ArrayList;
import org.bson.Document;
import com.develop.config.MongoDBConexion;
import com.develop.interfaces.PropietarioI;
import com.develop.modelos.PropietariosM;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;

public class PropietarioDAO implements PropietarioI{

	MongoClient mongoClient = new MongoDBConexion().getClient();
	
	@Override
	public String agregaPropietario(PropietariosM propietario) {
		MongoDatabase database = mongoClient.getDatabase("Residencias");
		MongoCollection<Document> collection = database.getCollection("Propietarios");
		Document d1 = new Document();
		String mensaje= "";
		try {
			d1.append("_id", propietario.getCurpPropietario());
			d1.append("nombre", propietario.getNombrePropietario());
			d1.append("apPaterno", propietario.getApPatPropietario());
			d1.append("apMaterno", propietario.getApMatPropietario());
			d1.append("edad", propietario.getEdadPropiertio());
			d1.append("telefono", propietario.getTelefonoPropierario());
			collection.insertOne(d1);
			System.out.println("Propietario agregado con �xito.");
			mensaje = "OK";
		}catch (Exception e) {
			System.out.println("No se pudo insetar valores: " + e.getMessage());
		}
		return mensaje;
	}

	@Override
	public String editaPropietario(PropietariosM propietario) {
		MongoDatabase database = mongoClient.getDatabase("Residencias");
		MongoCollection<Document> collection = database.getCollection("Propietarios");
		Document d1 = new Document();
		Document d2 = new Document();
		d2.append("_id", propietario.getCurpPropietario());
		String mensaje = "";
		try {
			if(collection.countDocuments(d2)==0) {
				mensaje = "No existe en la base de datos";
			}else {
				d1.append("_id", propietario.getCurpPropietario());
				d1.append("nombre", propietario.getNombrePropietario());
				d1.append("apPaterno", propietario.getApPatPropietario());
				d1.append("apMaterno", propietario.getApMatPropietario());
				d1.append("edad", propietario.getEdadPropiertio());
				d1.append("telefono", propietario.getTelefonoPropierario());
				collection.updateOne(d2, d1);
				System.out.println("Propietario actualizado exitosamente");
				mensaje = "OK";
			}
			return mensaje;
		}catch (Exception e) {
			System.out.println("No se pudo actualizar el propietario");
			return mensaje;
		}
	}

	@Override
	public String eliminaPropietario(String curp) {
		MongoDatabase database = mongoClient.getDatabase("Residencias");
		MongoCollection<Document> collection = database.getCollection("Propietarios");
		String mensaje = "";
		try {
			collection.deleteOne(new Document("_id", curp));
			System.out.println("Propietario eliminado con �xito.");
		} catch (Exception e) {
			System.out.println("No se pudo eliminar: " + e.getMessage());
		}
		return mensaje;
	}

	@Override
	public PropietariosM buscaPropietario(String curp) {
		MongoDatabase database = mongoClient.getDatabase("Residencias");
		MongoCollection<Document> collection = database.getCollection("Propietarios");
		FindIterable<Document> iterable;
		PropietariosM propietario = new PropietariosM();
		try {
			iterable = collection.find(new Document("_id",curp));
			for(Document doc : iterable) {
			    propietario.setCurpPropietario(doc.getString("_id"));
			    propietario.setNombrePropietario(doc.getString("nombre"));
			    propietario.setApPatPropietario(doc.getString("apPaterno"));
			    propietario.setApMatPropietario(doc.getString("apMaterno"));
			    propietario.setEdadPropiertio(doc.getInteger("edad"));
			    propietario.setTelefonoPropierario(doc.getString("telefono"));
			}
			return propietario;
		}catch (Exception e) {
			System.out.println("No se encontr� al arrendatario");
			return null;
		}
	}

	@Override
	public ArrayList<PropietariosM> listaPropietario() {
		MongoDatabase database = mongoClient.getDatabase("Residencias");
		MongoCollection<Document> collection = database.getCollection("Propietarios");
		ArrayList<PropietariosM> propietarios = new ArrayList<>();
		FindIterable<Document> docs;
		try {
			docs = collection.find();
			for(Document doc: docs) {
				PropietariosM propietario = new PropietariosM();
				propietario.setCurpPropietario(doc.getString("_id"));
			    propietario.setNombrePropietario(doc.getString("nombre"));
			    propietario.setApPatPropietario(doc.getString("apPaterno"));
			    propietario.setApMatPropietario(doc.getString("apMaterno"));
			    propietario.setEdadPropiertio(doc.getInteger("edad"));
			    propietario.setTelefonoPropierario(doc.getString("telefono"));
			    propietarios.add(propietario);
			}
			System.out.println("Lista conpletada con �xito");
			return propietarios;
		}catch (Exception e) {
			System.out.println("No se pudo completar");
			return null;
		}
	}

}

