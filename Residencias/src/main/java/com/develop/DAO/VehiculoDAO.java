package com.develop.DAO;

import java.util.ArrayList;

import org.bson.Document;

import com.develop.modelos.VehiculoM;
import com.develop.config.MongoDBConexion;
import com.develop.interfaces.VehiculoI;
import com.mongodb.client.*;
import com.mongodb.client.result.UpdateResult;

public class VehiculoDAO implements VehiculoI {
	MongoClient mongoClient = new MongoDBConexion().getClient();

	@Override
	public String agregarVehiculo(VehiculoM vehiculo) {
			try {
		MongoDatabase database = mongoClient.getDatabase("Residencias");
		MongoCollection<Document> collection = database.getCollection("Vehiculos");
		
		Document d1 = new Document();
		
		d1.append("_id", vehiculo.getIdPlacas());
		d1.append("marca", vehiculo.getMarcaVehiculo());
		d1.append("color", vehiculo.getColorVehiculo());
		d1.append("modelo", vehiculo.getModelo());
		d1.append("propietario", vehiculo.getPropietario());
		
		collection.insertOne(d1);
		System.out.println("Vehiculo agregado exitosamente");
		return "Vehiculo agregado exitosamente";
		
		}catch(Exception e) {
		
			System.out.println("No se pudo insertar valores:");
			System.out.println(e.getMessage());
			return "No se pudo acceder a la Base de datos";	
		}
	}		

	@Override
	public String eliminarVehiculo(VehiculoM vehiculo) {
		try {
			MongoDatabase database = mongoClient.getDatabase("Residencias");
			MongoCollection<Document> collection = database.getCollection("Vehiculos");
			
			Document d1 = new Document();
			
			d1.append("placas", vehiculo.getIdPlacas());
			collection.deleteMany(d1);
			
			System.out.println("Se elimin� correctamente");
			return "Vehiculo eliminado";
			}catch(Exception e) {
			
				System.out.println("No se pudo eliminar el vehiculo");
				System.out.println(e.getMessage());			
				return "No se pudo conectar con la Base de datos";
			}
	}
	
	@Override
	public String editarVehiculo(VehiculoM vehiculo) {
		try {
			
			MongoDatabase database = mongoClient.getDatabase("Residencias");
			MongoCollection<Document> collection = database.getCollection("Vehiculos");
			
			Document d2 = new Document();
			d2.append("_id", vehiculo.getIdPlacas());

			if(collection.countDocuments(d2)==0) {
				return "No existe en la Base de datos";
			}
			
			Document d1 = new Document();
			
			d1.append("_id", vehiculo.getIdPlacas());
			d1.append("marca", vehiculo.getMarcaVehiculo());
			d1.append("color", vehiculo.getColorVehiculo());
			d1.append("modelo", vehiculo.getModelo());
						
			collection.updateOne(d2, d1);
			
			return "Vehiculo editado";
			}catch(Exception e) {
			
				System.out.println("No se pudo editar el vehiculo: " + vehiculo.getIdPlacas());
				System.out.println(e.getMessage());			
				return "No se pudo conectar con la Base de datos";
			}
		}

	@Override
	public VehiculoM buscarVehiculo(String placas) {
		try {
			MongoDatabase database = mongoClient.getDatabase("Residencias");
			MongoCollection<Document> collection = database.getCollection("Vehiculos");
			
			Document d1 = new Document();
			d1.append("_id", placas);
			FindIterable<Document> doc =collection.find(d1);
			
			for(Document d: doc) {
				VehiculoM auto = new VehiculoM();
				auto.setIdPlacas(d.getString("_id"));
				auto.setMarcaVehiculo(d.getString("marca"));
				auto.setModelo(d.getString("modelo"));
				auto.setColorVehiculo(d.getString("color"));
				auto.setPropietario(d.getString("propietario"));
				System.out.println("Se encotr�: "+ auto.getIdPlacas()+" correctamente");
				
				return auto;
			}
			}catch(Exception e) {
			
				System.out.println("No se pudo editar el vehiculo: " + placas);
				System.out.println(e.getMessage());
			}
		return null;
	}
	
	@Override
	public ArrayList<VehiculoM> listVehiculos() {
		try{
			MongoDatabase database = mongoClient.getDatabase("Residencias");
			MongoCollection<Document> collection = database.getCollection("Vehiculos");
			ArrayList<VehiculoM> vehiculos = new ArrayList<>();
			
			FindIterable<Document> docs = collection.find();
			for(Document d: docs) {
				VehiculoM auto = new VehiculoM();
				auto.setIdPlacas(d.getString("_id"));
				auto.setMarcaVehiculo(d.getString("marca"));
				auto.setModelo(d.getString("modelo"));
				auto.setColorVehiculo(d.getString("color"));
				auto.setPropietario(d.getString("propietario"));
				vehiculos.add(auto);
			}
			return vehiculos;
		}catch (Exception e) {
			System.out.println("Ocurri� un error al buscar en la BD");
		}
		return null;
	}
		
}
