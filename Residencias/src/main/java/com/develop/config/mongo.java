package com.develop.config;

import java.util.ArrayList;

import org.bson.Document;


import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;


public class mongo {

	public mongo() {
	try {
		ConnectionString connectionString = new ConnectionString("mongodb+srv://Fer:1234@cluster0.dzku2.mongodb.net/Residencias?retryWrites=true&w=majority");
		MongoClientSettings settings = MongoClientSettings.builder()
		        .applyConnectionString(connectionString)
		        .build();
		MongoClient mongoClient = MongoClients.create(settings);
		MongoDatabase database = mongoClient.getDatabase("Residencias");
		MongoCollection<Document> collection = database.getCollection("tabla");
		MongoCursor<Document> doc = collection.find().iterator();
		while(doc.hasNext()) {
			ArrayList<Object> veri = new ArrayList(doc.next().values());
			System.out.println(veri.get(0));
			System.out.println(veri.get(1));
			System.out.println(veri.get(2));
			System.out.println(veri.get(3));	
		}
		
		Document d1 = new Document("_id",1000);//Propiedad
		Document d2 = new Document("_id",1001);//Direccion
		Document d3 = new Document("_id",1002);//Propietario
		Document d4 = new Document("_id",1003);//Casetas
		Document d5 = new Document("_id",1004);//Arrendatario
		
		//Direccion
		d2.append("calle", "flores");
		d2.append("numero", "78");
		//Propietario
		d3.append("CURP","TUCD950425HDFRRG09");
		d3.append("Nombre", "Diego Alexis");
		d3.append("apPaterno", "Trujillo");
		d3.append("apMaterno", "Carcamo");
		d3.append("telefono","5569700741");
		//Casetas
		d4.append("nombreCaseta", "Caseta 2");
		//Arrendatarios
		d5.append("nombre", "Fer");
		d5.append("apPaterno", "Rodriguez");
		d5.append("apMaterno", "Alvarado");
		d5.append("Telefono", "5733662255");
		d5.append("fechaIni", "2020-03-04: 11:01:20");
		d5.append("fechaFin", "2023-04-06: 09:31:14");
		
		d1.append("direccion", d2);
		d1.append("propietario", d3);
		d1.append("casetas", d4);
		d1.append("arrendatario", d5);
		d1.append("renta", false);
		d1.append("venta", true);
		
		
		collection.insertOne(d1);
	}catch(Exception e) {
		System.out.println(e.getMessage());
		
	}
	}

}
