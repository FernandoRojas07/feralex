package com.develop.config;
import com.mongodb.ConnectionString;
import com.mongodb.MongoClientSettings;
import com.mongodb.client.*;

public class MongoDBConexion {
	 MongoClient cliente;
	 
	 public MongoDBConexion() {
		 try {
				ConnectionString connectionString = new ConnectionString("mongodb+srv://Fer:1234@cluster0.dzku2.mongodb.net/Residencias?retryWrites=true&w=majority");
				MongoClientSettings settings = MongoClientSettings.builder()
				        .applyConnectionString(connectionString)
				        .build();
				MongoClient mongoClient = MongoClients.create(settings);
				this.cliente = mongoClient;
			}catch(Exception e) {
				System.out.println(e.getMessage());
				
			}
	 }
	 public MongoClient getClient() {
		 return this.cliente;
	 }
}
